package com.JFColor.GUI;

import java.awt.Color;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GameFrame {

    private StandardFrame Frame;
    private JPanel GridTable;

    private JPanel [][] MainTable;
    private int NumberOfColors;
    private int Level;
    private String FilePath;

    private Color[] ColorsTable = new Color[10];


    public GameFrame( String Title, String FilePath, int L ) throws FileNotFoundException
    {
        ColorsTable[1] = Color.RED;
        ColorsTable[2] = Color.blue;
        ColorsTable[3] = Color.GREEN;
        ColorsTable[4] = Color.YELLOW;
        ColorsTable[5] = Color.ORANGE;
        ColorsTable[6] = Color.CYAN;
        ColorsTable[7] = Color.PINK;
        ColorsTable[8] = Color.MAGENTA;
        ColorsTable[9] = Color.WHITE;

        Level = L;
        this.FilePath = FilePath;
        GridTable = new JPanel(new GridLayout(Level+2, Level+2));
        MainTable = new JPanel[Level+1][Level+1];
        Frame = new StandardFrame(Title);

        BuildingFirstLevelFrame();
        FillingTable();
    }

    private void BuildingFirstLevelFrame()
    {
        if ( Level == 6 )
        {
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[1][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][1]);
            MainTable[1][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][2]);
            MainTable[1][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][3]);
            MainTable[1][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][4]);
            MainTable[1][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][5]);
            MainTable[1][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][6]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[2][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][1]);
            MainTable[2][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][2]);
            MainTable[2][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][3]);
            MainTable[2][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][4]);
            MainTable[2][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][5]);
            MainTable[2][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][6]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[3][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][1]);
            MainTable[3][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][2]);
            MainTable[3][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][3]);
            MainTable[3][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][4]);
            MainTable[3][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][5]);
            MainTable[3][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][6]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[4][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][1]);
            MainTable[4][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][2]);
            MainTable[4][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][3]);
            MainTable[4][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][4]);
            MainTable[4][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][5]);
            MainTable[4][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][6]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[5][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][1]);
            MainTable[5][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][2]);
            MainTable[5][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][3]);
            MainTable[5][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][4]);
            MainTable[5][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][5]);
            MainTable[5][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][6]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[6][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][1]);
            MainTable[6][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][2]);
            MainTable[6][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][3]);
            MainTable[6][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][4]);
            MainTable[6][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][5]);
            MainTable[6][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][6]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
        }

        else if ( Level == 7 )
        {
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[1][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][1]);
            MainTable[1][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][2]);
            MainTable[1][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][3]);
            MainTable[1][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][4]);
            MainTable[1][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][5]);
            MainTable[1][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][6]);
            MainTable[1][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][7]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[2][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][1]);
            MainTable[2][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][2]);
            MainTable[2][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][3]);
            MainTable[2][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][4]);
            MainTable[2][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][5]);
            MainTable[2][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][6]);
            MainTable[2][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][7]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[3][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][1]);
            MainTable[3][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][2]);
            MainTable[3][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][3]);
            MainTable[3][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][4]);
            MainTable[3][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][5]);
            MainTable[3][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][6]);
            MainTable[3][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][7]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[4][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][1]);
            MainTable[4][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][2]);
            MainTable[4][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][3]);
            MainTable[4][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][4]);
            MainTable[4][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][5]);
            MainTable[4][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][6]);
            MainTable[4][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][7]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[5][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][1]);
            MainTable[5][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][2]);
            MainTable[5][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][3]);
            MainTable[5][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][4]);
            MainTable[5][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][5]);
            MainTable[5][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][6]);
            MainTable[5][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][7]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[6][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][1]);
            MainTable[6][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][2]);
            MainTable[6][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][3]);
            MainTable[6][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][4]);
            MainTable[6][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][5]);
            MainTable[6][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][6]);
            MainTable[6][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][7]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[7][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][1]);
            MainTable[7][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][2]);
            MainTable[7][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][3]);
            MainTable[7][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][4]);
            MainTable[7][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][5]);
            MainTable[7][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][6]);
            MainTable[7][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][7]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
        }

        else if ( Level == 8 )
        {
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[1][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][1]);
            MainTable[1][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][2]);
            MainTable[1][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][3]);
            MainTable[1][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][4]);
            MainTable[1][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][5]);
            MainTable[1][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][6]);
            MainTable[1][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][7]);
            MainTable[1][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][8]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[2][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][1]);
            MainTable[2][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][2]);
            MainTable[2][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][3]);
            MainTable[2][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][4]);
            MainTable[2][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][5]);
            MainTable[2][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][6]);
            MainTable[2][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][7]);
            MainTable[2][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][8]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[3][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][1]);
            MainTable[3][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][2]);
            MainTable[3][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][3]);
            MainTable[3][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][4]);
            MainTable[3][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][5]);
            MainTable[3][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][6]);
            MainTable[3][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][7]);
            MainTable[3][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][8]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[4][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][1]);
            MainTable[4][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][2]);
            MainTable[4][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][3]);
            MainTable[4][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][4]);
            MainTable[4][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][5]);
            MainTable[4][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][6]);
            MainTable[4][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][7]);
            MainTable[4][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][8]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[5][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][1]);
            MainTable[5][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][2]);
            MainTable[5][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][3]);
            MainTable[5][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][4]);
            MainTable[5][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][5]);
            MainTable[5][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][6]);
            MainTable[5][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][7]);
            MainTable[5][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][8]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[6][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][1]);
            MainTable[6][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][2]);
            MainTable[6][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][3]);
            MainTable[6][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][4]);
            MainTable[6][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][5]);
            MainTable[6][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][6]);
            MainTable[6][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][7]);
            MainTable[6][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][8]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[7][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][1]);
            MainTable[7][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][2]);
            MainTable[7][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][3]);
            MainTable[7][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][4]);
            MainTable[7][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][5]);
            MainTable[7][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][6]);
            MainTable[7][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][7]);
            MainTable[7][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][8]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[8][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][1]);
            MainTable[8][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][2]);
            MainTable[8][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][3]);
            MainTable[8][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][4]);
            MainTable[8][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][5]);
            MainTable[8][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][6]);
            MainTable[8][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][7]);
            MainTable[8][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][8]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
        }

        else if ( Level == 9 )
        {
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[1][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][1]);
            MainTable[1][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][2]);
            MainTable[1][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][3]);
            MainTable[1][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][4]);
            MainTable[1][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][5]);
            MainTable[1][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][6]);
            MainTable[1][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][7]);
            MainTable[1][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][8]);
            MainTable[1][9] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[1][9]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[2][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][1]);
            MainTable[2][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][2]);
            MainTable[2][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][3]);
            MainTable[2][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][4]);
            MainTable[2][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][5]);
            MainTable[2][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][6]);
            MainTable[2][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][7]);
            MainTable[2][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][8]);
            MainTable[2][9] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[2][9]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[3][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][1]);
            MainTable[3][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][2]);
            MainTable[3][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][3]);
            MainTable[3][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][4]);
            MainTable[3][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][5]);
            MainTable[3][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][6]);
            MainTable[3][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][7]);
            MainTable[3][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][8]);
            MainTable[3][9] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[3][9]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[4][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][1]);
            MainTable[4][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][2]);
            MainTable[4][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][3]);
            MainTable[4][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][4]);
            MainTable[4][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][5]);
            MainTable[4][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][6]);
            MainTable[4][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][7]);
            MainTable[4][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][8]);
            MainTable[4][9] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[4][9]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[5][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][1]);
            MainTable[5][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][2]);
            MainTable[5][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][3]);
            MainTable[5][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][4]);
            MainTable[5][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][5]);
            MainTable[5][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][6]);
            MainTable[5][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][7]);
            MainTable[5][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][8]);
            MainTable[5][9] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[5][9]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[6][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][1]);
            MainTable[6][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][2]);
            MainTable[6][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][3]);
            MainTable[6][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][4]);
            MainTable[6][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][5]);
            MainTable[6][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][6]);
            MainTable[6][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][7]);
            MainTable[6][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][8]);
            MainTable[6][9] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[6][9]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[7][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][1]);
            MainTable[7][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][2]);
            MainTable[7][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][3]);
            MainTable[7][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][4]);
            MainTable[7][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][5]);
            MainTable[7][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][6]);
            MainTable[7][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][7]);
            MainTable[7][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][8]);
            MainTable[7][9] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[7][9]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[8][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][1]);
            MainTable[8][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][2]);
            MainTable[8][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][3]);
            MainTable[8][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][4]);
            MainTable[8][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][5]);
            MainTable[8][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][6]);
            MainTable[8][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][7]);
            MainTable[8][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][8]);
            MainTable[8][9] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[8][9]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            MainTable[9][1] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[9][1]);
            MainTable[9][2] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[9][2]);
            MainTable[9][3] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[9][3]);
            MainTable[9][4] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[9][4]);
            MainTable[9][5] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[9][5]);
            MainTable[9][6] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[9][6]);
            MainTable[9][7] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[9][7]);
            MainTable[9][8] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[9][8]);
            MainTable[9][9] = SquarePanel(Color.BLACK, true);
            GridTable.add(MainTable[9][9]);
            GridTable.add(SquarePanel(Color.BLACK, false));

            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
            GridTable.add(SquarePanel(Color.BLACK, false));
        }

        Frame.Frame.add(GridTable);
    }

    private JPanel SquarePanel( Color C, boolean b ) {

        JPanel Sq = new JPanel();
        Sq.setBackground(C);

        if ( b )
            Sq.setBorder(BorderFactory.createLineBorder(Color.WHITE));
        return Sq;
    }

    private void Hall(int x, int y)
    {
        JLabel Label = new JLabel("HALL");
        Label.setForeground(Color.WHITE);
        MainTable[x][y].add(Label);
    }

    private void Bridge(int x, int y)
    {
        JLabel Label = new JLabel("BRIDGE");
        Label.setForeground(Color.WHITE);
        MainTable[x][y].add(Label);
    }

    private void FillingTable() throws FileNotFoundException
    {
        int j, x1, x2, y1, y2;

        Scanner File = new Scanner(new File(FilePath));

        Level = File.nextInt();
        NumberOfColors = File.nextInt();

        for ( int i = 1 ; i <= NumberOfColors ; i++ )
        {
            j = File.nextInt();
            x1 = File.nextInt();
            y1 = File.nextInt();
            x2 = File.nextInt();
            y2 = File.nextInt();

            MainTable[x1][y1].setBackground(ColorsTable[j]);
            MainTable[x2][y2].setBackground(ColorsTable[j]);
        }

        while(File.hasNextInt())
        {
            j = File.nextInt();
            x1 = File.nextInt();
            y1 = File.nextInt();

            if ( j == 1 )
                Bridge(x1, y1);
            else if ( j == 2 )
                Hall(x1, y1);
        }
    }
}

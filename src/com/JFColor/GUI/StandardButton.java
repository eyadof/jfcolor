package com.JFColor.GUI;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StandardButton {

    public JPanel Panel;
    private JLabel Label;

    private int XSize;
    private int YSize;
    private int XLocation;
    private int YLocation;

    public StandardButton( String ButtonText, int XSize, int YSize, int XLocation, int YLocation )
    {
        Panel = new JPanel();
        Label = new JLabel(ButtonText);

        this.XSize = XSize;
        this.YSize = YSize;
        this.XLocation = XLocation;
        this.YLocation = YLocation;

        BuildingButton();
    }

    private void BuildingButton()
    {
        Panel.setSize(XSize, YSize);
        Panel.setLocation(XLocation, YLocation);
        Panel.setBackground(Color.DARK_GRAY);
        Panel.setBorder(BorderFactory.createLineBorder(Color.WHITE));

        Label.setForeground(Color.WHITE);
        Label.setVisible(true);

        Panel.setVisible(true);
        Panel.add(Label);

        FocusOnButton();
    }

    private void FocusOnButton()
    {
        Panel.addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent e) {
                Panel.setBorder(BorderFactory.createLineBorder(Color.RED));
                Label.setForeground(Color.RED);
            } });

        Panel.addMouseListener(new MouseAdapter() {
            public void mouseExited(MouseEvent e) {
                Panel.setBorder(BorderFactory.createLineBorder(Color.WHITE));
                Label.setForeground(Color.WHITE);
            } });
    }
}

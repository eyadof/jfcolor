package com.JFColor.GUI;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class StandardFrame {

    public JFrame Frame;

    public StandardFrame( String Title )
    {
        Frame = new JFrame( Title );
        BuildingFrame();
    }

    private void BuildingFrame()
    {
        Frame.setSize(600, 600);
        Frame.getContentPane().setBackground( Color.BLACK);
        Frame.setLocationRelativeTo ( null );
        Frame.setResizable(false);
        Frame.setVisible(true);
    }
}

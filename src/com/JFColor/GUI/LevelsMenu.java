package com.JFColor.GUI;

import java.awt.Color;
import javax.swing.JPanel;

public class LevelsMenu {

    private StandardFrame Frame;
    private StandardButton FirstLevelButton;
    private StandardButton SecondLevelButton;
    private StandardButton ThirdLevelButton;
    private StandardButton FourthLevelButton;
    private JPanel MainPanel;

    public LevelsMenu()
    {
        Frame = new StandardFrame("Select Levels");
        FirstLevelButton = new StandardButton("First Level ( 6 X 6 )", 300, 25, 150, 200);
        SecondLevelButton = new StandardButton("Second Level ( 7 X 7 )", 300, 25, 150, 250);
        ThirdLevelButton = new StandardButton("Third Level ( 8 X 8 )", 300, 25, 150, 300);
        FourthLevelButton = new StandardButton("Fourth Level ( 9 X 9 )", 300, 25, 150, 350);

        BuildingLevelsMenu();
    }

    private void BuildingLevelsMenu()
    {
        MainPanel = new JPanel();
        MainPanel.setLayout(null);
        MainPanel.setBackground(Color.BLACK);

        MainPanel.add(FirstLevelButton.Panel);
        MainPanel.add(SecondLevelButton.Panel);
        MainPanel.add(ThirdLevelButton.Panel);
        MainPanel.add(FourthLevelButton.Panel);

        Frame.Frame.add(MainPanel);
    }
}

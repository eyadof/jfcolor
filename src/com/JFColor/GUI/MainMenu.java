package com.JFColor.GUI;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class MainMenu {

    private StandardFrame Frame;
    private StandardButton PlayButton;
    private StandardButton HowToPlayButton;
    private StandardButton AboutButton;
    private JPanel MainPanel;

    public MainMenu()
    {
        Frame = new StandardFrame("Main Menu");
        PlayButton = new StandardButton("PLAY", 300, 25, 150, 200);
        HowToPlayButton = new StandardButton("HOW TO PLAY", 300, 25, 150, 250);
        AboutButton = new StandardButton("ABOUT", 300, 25, 150, 300);

        BuildingMainMenu();
    }

    private void BuildingMainMenu()
    {
        MainPanel = new JPanel();
        MainPanel.setLayout(null);
        MainPanel.setBackground(Color.BLACK);

        MainPanel.add(PlayButton.Panel);
        MainPanel.add(HowToPlayButton.Panel);
        MainPanel.add(AboutButton.Panel);

        Frame.Frame.add(MainPanel);
    }
}

package com.JFColor.exception;

public class WrongDestination extends Exception {
    public WrongDestination(){
        super();
    }
    public WrongDestination(String message){
        super(message);
    }
}

package com.JFColor.exception;


public class AccessingHall extends Exception {
    public AccessingHall(){
        super();
    }

    public AccessingHall(String message){
        super(message);
    }
}

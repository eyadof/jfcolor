package com.JFColor.exception;


public class WrongMove extends Exception {

    public WrongMove(){
        super();
    }
    public WrongMove(String message){
        super(message);
    }
}

package com.JFColor.controller;

import com.JFColor.base.*;
import com.JFColor.exception.*;


public class game {
    private cell[][] grid;
    private point[] points;
    private boolean finish;

    public game(){
        finish = false;
    }

    public void updateGrid(int x1,int y1,int x2,int y2){
        cell old = grid[x1][y1];
        cell current = grid[x2][y2];
        String Mtype = (old.getX() == current.getX())?"vertical":"horizontal";

        if (old.equals(current)){
            throw new WrongMove();
        }

        if (old.adjacent(current)){
            throw new WrongMove();
        }

        if (current.getChild() == null){
            switch (old.getChild().getClass().toString()){
                case "point":
                    CreateRoute(current,old.getChild());
                    CreatePipe(current,old.getChild());
                    grid[x2][y2] = current;
                    break;
                case "pipe":
                    CreatePipe(current,old.getChild());
                    grid[x2][y2] = current;
                    break;
                case "bridge":
                    bridge b = (bridge) old.getChild();
                    if (Mtype == "vertical"){
                        CreatePipe(current,b.getVertical());
                        grid[x2][y2] = current;
                    }
                    else{
                        CreatePipe(current,b.getHorizontal());
                        grid[x2][y2] = current;
                    }
                    break;
            }
        }
        else{
            switch (current.getChild().getCell().toString()){
                case "point":
                    if (old.getChild().getClass().toString() == "point"){
                        point p = (point) old.getChild();
                        if (p.getPeer() == current.getChild()){
                            p.getRoute().close(p);
                        }
                        else
                            throw new WrongDestination();
                    }
                    else if(old.getChild().getClass().toString() == "pipe"){
                        pipe p = (pipe) old.getChild();
                        if (p.getRoute().getDest() == current.getChild()){
                            p.getRoute().close(current.getChild());
                        }
                        else
                            throw new WrongDestination();
                    }
                    break;
                case "pipe":
                    pipe p = (pipe) current.getChild();
                    p.getRoute().removeLast(p.getRoute().indexOf(p));
                    CreatePipe(current,old.getChild());
                    break;
                case "bridge":
                    bridge b = (bridge) current.getChild();
                    b.addPipe((pipe)old.getChild());
                    break;
                case "hall":
                    throw new AccessingHall();
                    break;
            }
        }
        if (CheckGameEnd()){
            finish = true;
        }

    }

    private boolean CheckGameEnd(){
        for (int i = 0;i<grid.length;i++){
           for (int j=0;j<grid[i].length;j++){
               if (grid[i][j] == null){
                   return false;
               }
           }
        }
        for (int i=0;i<points.length;i++){
            if (points[i].getRoute().isFinish() == false){
                return false;
            }
        }
        return true;
    }

    private void CreateRoute(cell c,jfcobject point){
        if (point.getCell().toString() == "point" ){
            point po = (point) point;
            route r = new route(po);
        }
    }
    private void CreatePipe(cell c,jfcobject p){
        if(p.getClass().toString() == "point"){
            point po = (point)p;
            pipe t = new pipe(c, po.getRoute());
        }
        else if (p.getClass().toString() == "pipe"){
            pipe pi = (pipe) p;
            pipe t = new pipe(c,pi.getRoute());
        }
    }
    public boolean isFinish(){
        return finish;
    }

}
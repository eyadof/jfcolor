package com.JFColor.base;
import java.util.ArrayList;

public class route {
    private ArrayList<pipe> pipes;
    private point src;
    private point dest;
    private boolean finish;

    public route(point p){
        this.src = p;
        this.dest= p.getPeer();
        pipes = new ArrayList<>();
        this.src.setRoute(this);
        this.finish = false;
    }
    public point getSrc() {
        return src;
    }

    public point getDest() {
        return dest;
    }

    public void addPipe(pipe pipe){
        if (!finish){
            this.pipes.add(pipe);
        }
    }

    public void removePipe(pipe pipe){
        this.pipes.remove(pipe);
    }

    public void removeLast(){
        this.pipes.remove(this.pipes.size());
    }

    public void removeLast(int index){
        for (int i = index;i < this.pipes.size();i++ ){
            this.pipes.get(i).removeFromCell();
            this.pipes.remove(i);
        }
    }

    public void close(jfcobject point){
        if (point.getClass() == dest.getClass() ){
            point p = (point) point;
            if (p.equals(this.dest)){
                this.finish = true;
            }
        }
    }

    public pipe lastPipe(){
        return pipes.get(pipes.size());
    }

    public int indexOf(pipe p){
        return pipes.indexOf(p);
    }

    public boolean isFinish(){
        return finish;
    }

}

package com.JFColor.base;


public class cell {

    private int x;
    private int y;
    private jfcobject child;

    public cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public jfcobject getChild() {
        return child;
    }

    public void setChild(jfcobject child) {
        this.child = child;
    }

    public  boolean adjacent(cell c2){
        if ((this.x == c2.getX()+1) && (this.y == c2.getY()+1)){
            return true;
        }
        if ((this.x == c2.getX()+1)&&(this.y == c2.getY()-1)){
            return true;
        }
        if ((this.x == c2.getX()-1)&&(this.y == c2.getY()+1)){
            return true;
        }
        if ((this.x == c2.getX()-1)&&(this.y == c2.getY()-1)){
            return true;
        }
        return false;
    }

    @Override public boolean equals(Object o){
        if (o == null){
            return false;
        }
        if (getClass() != o.getClass()){
            return false;
        }
        final cell other = (cell) o;

        if ( (this.x == other.getX()) && (this.y == other.getY()) ){
            return true;
        }
        return false;
    }
}

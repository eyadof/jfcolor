package com.JFColor.base;

public class pipe extends jfcobject{
    private route route;
    private String  direction;

    public pipe(cell c,route route){
        setCell(c);
        setColor(this.route.getDest().getColor());
        this.route = route;
        route.addPipe(this);
        this.setDirection();
    }

    public route getRoute(){
        return route;
    }

    private void setDirection(){
        if (this.route.lastPipe().getCell().getX() == this.getCell().getX()){
            direction = "vertical";
        }
        else
            direction = "horizontal";
    }

    public String getDirection(){
        return direction;
    }

    public void removeFromCell(){
        this.getCell().setChild(null);
    }
}

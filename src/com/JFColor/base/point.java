package com.JFColor.base;

public class point extends jfcobject {

    private point peer;
    private route route;

    public point(){}

    public point(cell c,String color ,point peer){
        setCell(c);
        setColor(color);
        this.peer = peer;
    }


    public point getPeer() {
        return peer;
    }

    public void setPeer(point peer) {
        this.peer = peer;
    }

    public route getRoute(){
        return route;
    }

    public void setRoute(route route){
        this.route = route;
    }

    @Override public boolean equals(Object o){
        if (o == null){
            return false;
        }
        if (getClass() != o.getClass()){
            return false;
        }
        final point other = (point) o;

        if ( (this.route == other.route) && (this.getColor() == other.getColor()) ){
            return true;
        }
        return false;
    }

}

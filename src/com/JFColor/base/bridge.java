package com.JFColor.base;

public class bridge extends jfcobject {

    private pipe horizontal;
    private pipe vertical;

    public bridge(cell c) {
        this.setCell(c);
    }

    public pipe getHorizontal(){
        return horizontal;
    }

    public pipe getVertical(){
        return vertical;
    }

    public boolean isEmpty(){
        if ((this.horizontal != null) || (this.vertical != null)){
            return false;
        }
        return true;
    }

    public void addPipe(pipe p) {
        if (p.getDirection() == "horizontal") {
            if (this.horizontal == null) {
                horizontal = p;
            }
        }
        else {
            if (this.vertical == null) {
                vertical = p;
            }
        }
    }
}

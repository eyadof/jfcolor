package com.JFColor.base;

public abstract class jfcobject {

    private cell cell;
    private String color;

    public cell getCell() {
        return cell;
    }

    public void setCell(cell cell) {
        this.cell = cell;
        cell.setChild(this);
    }

    public void setColor(String nv){
        color = nv;
    }

    public String getColor(){
        return color;
    }
}
